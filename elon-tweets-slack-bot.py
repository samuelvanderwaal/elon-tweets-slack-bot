import os
import time
import twitter
from slackclient import SlackClient

def post_tweet(tweet):
	if slack_client.rtm_connect():
		slack_client.api_call("chat.postMessage", channel=MUSKIAN,
                          text=tweet, as_user=True)
	else:
		log.write("Connection failed!")
		log.close()

# Error log
log = open("log.txt", "w")

# Useful constants
ONE_HOUR = 60*60*1
SIX_HOURS = 60*60*6
TWENTY_FOUR_HOURS = 60*60*24

# Twitter Access Credentials
CONSUMER_KEY = os.environ.get("ELON_TWEET_CONSUMER_KEY")
CONSUMER_SECRET = os.environ.get("ELON_TWEET_CONSUMER_SECRET")
APPLICATION_TOKEN = os.environ.get("ELON_TWEET_APP_TOKEN")
APPLICATION_TOKEN_SECRET = os.environ.get("ELON_TWEET_APP_TOKEN_SECRET")

# Elon's Twitter Handle
ELON_MUSK = "@elonmusk"

# Slack Access Credentials
BOT_ID = os.environ.get("ELON_TWEET_BOT_ID")
BOT_TOKEN = os.environ.get("ELON_TWEET_BOT_TOKEN")

# Slack Channel ID
MUSKIAN = "C4780FSSJ"

# Test Channel
TEST_CHANNEL = "G7AKBH5SP"

# Instantiate Twitter App
api = twitter.Api(consumer_key=CONSUMER_KEY,
                  consumer_secret=CONSUMER_SECRET,
                  access_token_key=APPLICATION_TOKEN,
                  access_token_secret=APPLICATION_TOKEN_SECRET)

# Instantiante Slack Client
slack_client = SlackClient(BOT_TOKEN)

statuses = api.GetUserTimeline(screen_name=ELON_MUSK)

time_now = time.time()

last_hour = time_now - ONE_HOUR

for status in statuses:
	if last_hour < status.created_at_in_seconds < time_now:
		tweet = status.text
		post_tweet(tweet)